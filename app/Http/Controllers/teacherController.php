<?php

namespace App\Http\Controllers;

use App\Models\teacher;
use Illuminate\Http\Request;

class teacherController extends Controller
{
    public function home(){
        return view('welcome');
    } 
    public function show(){
        $roles = teacher::get();
        return view('show', compact('roles'));
    }
    public function store(Request $request){
        
        $fileName=time().'ek'.$request->file('image')->getClientOriginalName();
        $path=$request->file('image')->storeAs('uploads',$fileName,'public');
        $requestData='/storage/'.$path;
        //command:php artisan storage:link
        teacher::create([
           'image'=>$requestData,
           'Fname'=>$request->name,
        ]);
        return redirect()->route('show');
    }   
}
